import { createSlice, Dispatch } from '@reduxjs/toolkit';
import { Message, MessageState } from '../types';

const initialState: MessageState = {
  messages: [],
  error: null,
  loading: false,
};

const messageSlice = createSlice({
  name: 'message',
  initialState,
  reducers: {
    setMessages: (state, action) => {
      state.messages = action.payload as Message[];
    },
    addMessage: (state, action) => {
      state.messages.push(action.payload as Message);
    },
    sendMessage: (state, action) => {
      // Placeholder for own message
    },
    setLoading: (state) => {
      state.loading = true;
    },
    setLoadingComplete: (state) => {
      state.loading = false;
    },
  },
});

export const messageReducer = messageSlice.reducer;
export const {
  addMessage,
  sendMessage,
  setMessages,
  setLoading,
  setLoadingComplete,
} = messageSlice.actions;

// Action
export function getMessages() {
  return async (dispatch: Dispatch, getState: () => {}) => {
    dispatch(setLoading());

    try {
      const res = await fetch('/api/messages');
      const data = await res.json();
      dispatch(setMessages(data.messages));
    } catch (e) {
      console.log(e);
    } finally {
      dispatch(setLoadingComplete());
    }
  };
}
