import { createSlice, Dispatch } from '@reduxjs/toolkit';
import { User, UserState } from '../types';

const initialState: UserState = {
  currentUser: null,
  isAuthenticated: false,
  users: [],
  onlineUsers: [],
  loading: false,
  error: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    login: (state, action) => {
      state.currentUser = action.payload as User;
      state.isAuthenticated = true;
    },
    logout: (state) => {
      state.currentUser = null;
      state.isAuthenticated = false;
    },
    setUsers: (state, action) => {
      state.users = action.payload as User[];
    },
    setOnlineUsers: (state, action) => {
      state.onlineUsers = action.payload as string[]; // username list
    },
    addUser: (state, action) => {
      state.users.push(action.payload as User);
    },
    setLoading: (state) => {
      state.loading = true;
    },
    setLoadingComplete: (state) => {
      state.loading = false;
    },
  },
});

export const userReducer = userSlice.reducer;
export const {
  login,
  logout,
  setUsers,
  addUser,
  setOnlineUsers,
  setLoading,
  setLoadingComplete,
} = userSlice.actions;

// Action
export function getUsers() {
  return async (dispatch: Dispatch, getState: () => {}) => {
    dispatch(setLoading());

    try {
      const res = await fetch('/api/users');
      const data = await res.json();

      dispatch(setUsers(data.users));
    } catch (e) {
      // Not handling errors
      console.log(e);
    } finally {
      dispatch(setLoadingComplete());
    }
  };
}
