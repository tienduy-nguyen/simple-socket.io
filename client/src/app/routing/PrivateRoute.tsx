import React, { Component } from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { RootState } from '../stores/rootReducer';

interface Props extends RouteProps {
  component: any;
}

export const PrivateRoute: React.FC<Props> = ({
  component: Component,
  ...rest
}) => {
  const { isAuthenticated } = useSelector((state: RootState) => state.user);
  return (
    <Route
      render={(props) =>
        isAuthenticated ? <Component {...props} /> : <Redirect to='/' />
      }
      {...rest}
    />
  );
};
