import reactRefresh from '@vitejs/plugin-react-refresh';
import { UserConfig } from 'vite';
import { resolve } from 'path';

const serverUrl = 'http://localhost:5000';

console.log('serverUrl: --------------> ', serverUrl);

const config: UserConfig = {
  plugins: [reactRefresh()],
  resolve: {
    alias: {
      src: resolve(__dirname, 'src'),
    },
  },
  server: {
    proxy: {
      '/api': {
        target: serverUrl,
        changeOrigin: true,
        secure: false,
      },
    },
  },
};

export default config;
