const defaultConfig = require('tailwindcss/defaultConfig');

module.exports = {
  purge: ['index.html', 'src/**/*.tsx'],
  theme: {
    fontFamily: {
      sans: ['Inter var', defaultConfig.theme.fontFamily.sans],
    },
  },
  darkMode: 'media',
};
