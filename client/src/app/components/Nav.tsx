import React from 'react';
import logo from '/android-chrome-192x192.png';

interface NavProps {
  onClick: () => void;
}

export const Nav: React.FC<NavProps> = ({ onClick }) => {
  return (
    <div className='fixed w-full top-0 left-0 z-10 flex items-center justify-between shadow-sm py-3 px-6 nav'>
      <div className='flex items-center'>
        <img src={logo} alt='chat app' className='h-12 w-12' />
        <h1 className='ml-3 text-3xl font-semibold'>Chat App</h1>
      </div>
      <button
        type='button'
        onClick={onClick}
        className='px-6 py-3 text-white bg-blue-500 font-medium rounded-md shadow-md hover:bg-blue-dark disabled:opacity-50 focus:outline-none'
      >
        Logout
      </button>
    </div>
  );
};
