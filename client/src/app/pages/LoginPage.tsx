import React from 'react';
import { Login } from '../components/Login';

export const LoginPage = () => {
  return (
    <div className=' mt-5 mx-auto'>
      <Login />
    </div>
  );
};
