export const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
export const usernameRegex = /[^A-Za-z0-9_-]/g;
export function validateEmail(email: string) {
  if (!emailRegex.test(email)) return 'Input must be an email';
}

export function validateUsername(username: string) {
  if (!username.trim()) return 'Username is required';
  if (usernameRegex.test(username))
    return 'Username can only contain letters, numbers, dashes and underscore';
}
