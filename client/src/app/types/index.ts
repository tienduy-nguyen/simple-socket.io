export interface User {
  email: string;
  username: string;
  online?: boolean;
}
export interface UserState {
  users: User[];
  onlineUsers: string[];
  currentUser: User | null | undefined;
  isAuthenticated: boolean;
  loading: boolean;
  error: string | null;
}

export interface Message {
  content: string;
  date: string;
  author: string;
}
export interface MessageState {
  messages: Message[];
  error: string | null;
  loading: boolean;
}
