import { rootReducer } from './rootReducer';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { SocketClient } from '../features/socketClient';
import { socketMiddleware } from '../features/socketMiddleware';
import { useDispatch } from 'react-redux';

const socket = new SocketClient();

export const store = configureStore({
  reducer: rootReducer,
  middleware: [socketMiddleware(socket), ...getDefaultMiddleware()],
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
