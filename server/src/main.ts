import express, { Express, Request, Response } from 'express';
import http from 'http';
import socketio from 'socket.io';
import cors from 'cors';
import { CLIENT_HOST, PORT } from './configs';
import { Message, Session, User } from './types';
import { getUserByUsername } from './utils/getUser';

const app: Express = express();

// Setup http server and socket server
const server: http.Server = http.createServer(app);
const io: socketio.Server = new socketio.Server(server, {
  cors: {
    origin: CLIENT_HOST,
    credentials: true,
  },
});

app.use(cors());

let messages: Message[] = [];
let users: User[] = [];
let activeUsers: Session[] = [];

app.get('/', (req, res) => {
  res.send('Hi there! Welcome from socketio nodejs.');
});
app.get('/api/messages', (req: Request, res: Response) => {
  res.send({ messages });
});

app.get('/api/users', (req, res) => {
  res.send({ users });
});

io.on('connection', (socket: any) => {
  const id = socket.id;
  console.log(`New client session: ${id}`);

  // New login
  socket.on('new login', (user: User) => {
    console.log(`user connected: ${user.username}`);

    // Add the new login to the list of users
    if (!users.some((u) => u.username === user.username)) {
      users = [...users, user];
      io.emit('new user added', user);
    }

    // Save the current username/session combination
    socket.sessionUsername = user.username;
    activeUsers.push({
      session: id,
      username: user.username,
    });

    io.emit('users online', getUserByUsername(activeUsers));
  });

  // send message
  socket.on('send message', (message: Message) => {
    console.log(`message: ${message.author}: ${message.content}`);
    messages.push(message);
    io.emit('receive message', message);
  });

  // disconnect
  socket.on('disconnect', () => {
    console.log(`user disconnected: ${socket.sessionUsername}`);

    // remove the current session
    // The same user may have multiple client sessions open so this prevents incorrect display
    activeUsers = activeUsers.filter(
      (user) =>
        !(user.username === socket.sessionUsername && user.session === id)
    );
    io.emit('users online', getUserByUsername);
  });
});

app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});
