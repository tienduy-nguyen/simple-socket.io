import React from 'react';
export const NotFoundPage = () => {
  return (
    <div className='mt-5'>
      <h1>404 - not found</h1>
      <p>Page seems not exist</p>
    </div>
  );
};
