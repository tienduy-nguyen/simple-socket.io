import { Session } from 'src/types';

export function getUserByUsername(activeUsers: Session[]): string[] {
  return [...new Set(activeUsers.map((user) => user.username))];
}
