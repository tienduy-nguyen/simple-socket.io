import React, { useState } from 'react';
import { login } from '../features/user.slice';
import { useAppDispatch } from '../stores/store';
import { validateEmail, validateUsername } from '../utils/validation';
import { TextField } from './TextField';
import logo from '/android-chrome-192x192.png';

interface FormFields {
  email: string;
  username: string;
}
interface FormErrors {
  email?: string;
  username?: string;
}

export const Login = () => {
  const [user, setUser] = useState<FormFields>({ email: '', username: '' });
  const [errors, setErrors] = useState<FormErrors>({});
  const dispatch = useAppDispatch();

  const validate = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setErrors({ ...errors, [name]: validateEmail(value) });
        break;
      case 'username':
        setErrors({ ...errors, [name]: validateUsername(value) });
        break;

      default:
        break;
    }
  };

  const isLoginButtonDisabled = () => {
    if (!user.email || !user.username) return true;
    if (!!errors.email || !!errors.username) return true;
    return false;
  };

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
    if (errors[name as keyof FormErrors]) validate(name, value);
  };

  const handleBlur = (e: any) => {
    const { name, value } = e.target;
    validate(name, value);
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    dispatch(login(user));
    localStorage.setItem('user', JSON.stringify(user));
  };

  return (
    <div className='flex justify-center items-center h-screen bg-gray-100'>
      <div className='container bg-white m-4 p-12 text-center rounded-md shadow-lg max-w-full-sm max-w-md mx-auto'>
        <img
          src={logo}
          className='block mx-auto mb-3 text-center w-24'
          alt='Doge'
        />

        <h1 className='text-3xl mb-8 font-bold text-gray-700'>Chat Sign In</h1>

        <form onSubmit={handleSubmit}>
          <TextField
            label='Email Address'
            name='email'
            value={user.email}
            placeholder='doge@example.com'
            onChange={handleChange}
            onBlur={handleBlur}
            error={errors.email}
          />
          <TextField
            label='Username'
            name='username'
            value={user.username}
            placeholder='floppydiskette'
            onChange={handleChange}
            onBlur={handleBlur}
            error={errors.username}
          />
          <div className='mb-3'>
            <button
              className='w-full px-3 py-4 text-white bg-blue-500 font-medium rounded-md shadow-md hover:bg-blue-dark disabled:opacity-50 focus:outline-none'
              disabled={isLoginButtonDisabled()}
            >
              Sign in
            </button>
          </div>
          <p className='text-left text-gray-400 text-sm'>
            Don't have an account yet? That's fine.
          </p>
        </form>
      </div>
    </div>
  );
};
