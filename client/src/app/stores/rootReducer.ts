import { combineReducers } from 'redux';
import { messageReducer } from '../features/message.slice';
import { userReducer } from '../features/user.slice';

export const rootReducer = combineReducers({
  user: userReducer,
  message: messageReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
