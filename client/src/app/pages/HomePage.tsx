import React from 'react';
import { Chat } from '../components/Chat';

export const HomePage = () => {
  return (
    <div>
      <Chat />
    </div>
  );
};
