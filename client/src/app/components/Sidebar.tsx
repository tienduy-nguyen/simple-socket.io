import React from 'react';
import { User } from '../types';

interface SidebarProps {
  currentUser: User | null | undefined;
  users: User[];
}

export const Sidebar: React.FC<SidebarProps> = ({ users, currentUser }) => {
  return (
    <div className='flex-none min-w-300 bg-blue-500 overflow-y-auto'>
      <div>
        <h2 className='m-6 text-white font-bold text-lg'>Users</h2>
        {users.map((user, index) => (
          <div
            key={index}
            className='p-3 mx-6 my-2 text-white text-opacity-70 bg-blue-dark rounded-md'
          >
            <div className='flex items-center'>
              <div
                className={`h-2 w-2 mr-2 rounded-full inline-block ${
                  user.online ? 'bg-green-400' : 'bg-blue-light'
                }`}
              ></div>
              <span>{user.username}</span>
              {user.username === currentUser!.username && (
                <span className='ml-1 text-white text-opacity-30'>(you)</span>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
