import { io, Socket } from 'socket.io-client';
import { WEB_SOCKET_HOST } from '../constants';

export class SocketClient {
  socket: Socket | null | undefined;
  // constructor() {
  //   this.socket = io(WEB_SOCKET_HOST);
  // }

  connect() {
    this.socket = io(WEB_SOCKET_HOST);
  }

  disconnect() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }
  emit(event: string, data: any) {
    if (this.socket) {
      this.socket.emit(event, data);
    }
  }

  on(event: string, func: any) {
    if (this.socket) {
      this.socket.on(event, func);
    }
  }
}
