import { Dispatch } from 'redux';
import { RootState } from '../stores/rootReducer';
import { Message, User } from '../types';
import { addMessage } from './message.slice';
import { SocketClient } from './socketClient';
import { addUser, setOnlineUsers } from './user.slice';

interface SocketMiddlewareParams {
  dispatch: Dispatch;
  getState: () => RootState;
}
export function socketMiddleware(socket: SocketClient) {
  return (params: SocketMiddlewareParams) => (next: any) => (action: any) => {
    const { dispatch } = params;
    const { type, payload } = action;
    switch (type) {
      case 'users/login': {
        socket.connect();

        // Update the online users list every time a user logs in or out
        socket.on('users online', (onlineUsers: string[]) => {
          dispatch(setOnlineUsers(onlineUsers));
        });

        // Append a message every time a new one comes in
        socket.on('receive message', (message: Message) => {
          dispatch(addMessage(message));
        });

        // Append a user every time a new one is registered
        socket.on('new user added', (user: User) => {
          dispatch(addUser(user));
        });

        // Add the current user tot he online user lis
        socket.emit('new login', payload);
        break;
      }
      case 'users/logout': {
        socket.disconnect();
        break;
      }
      case 'messages/sendMessage': {
        socket.emit('send message', payload);
        break;
      }
      default:
        break;
    }

    return next(action);
  };
}
