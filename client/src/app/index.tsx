import React, { useEffect, useState } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { login } from './features/user.slice';
import { HomePage, LoginPage, NotFoundPage } from './pages';
import { PrivateRoute } from './routing/PrivateRoute';
import { PublicRoute } from './routing/PublicRoute';
import { useAppDispatch } from './stores/store';

function App() {
  const dispatch = useAppDispatch();
  const [checkedUser, setCheckedUser] = useState(false);
  useEffect(() => {
    const loggedInUser = localStorage.getItem('user');
    if (loggedInUser) {
      dispatch(login(JSON.parse(loggedInUser)));
    }
    setCheckedUser(true);
  }, [setCheckedUser, dispatch]);

  if (!checkedUser) {
    return (
      <div className='flex justify-center items-center h-screen bg-gray-100'>
        Loading ...
      </div>
    );
  }
  return (
    <BrowserRouter>
      <div className='App'>
        <Switch>
          <PrivateRoute exact path='/chat' component={HomePage} />
          <PublicRoute exact path='/' component={LoginPage} />
          <PublicRoute component={NotFoundPage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
