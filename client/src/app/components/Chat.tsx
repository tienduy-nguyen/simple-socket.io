import dayjs from 'dayjs';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  addMessage,
  getMessages,
  sendMessage,
} from '../features/message.slice';
import { getUsers, logout } from '../features/user.slice';
import { RootState } from '../stores/rootReducer';
import { Message } from '../types';
import { ChatArea } from './ChatArea';
import { Nav } from './Nav';
import { Sidebar } from './Sidebar';

export const Chat = () => {
  const dispatch = useDispatch();
  const [messageInput, setMessageInput] = useState('');
  const {
    currentUser,
    users,
    loading: usersLoading,
    onlineUsers,
  } = useSelector((state: RootState) => state.user);
  const { messages, loading: messagesLoading } = useSelector(
    (state: RootState) => state.message
  );

  const handleLogoutClick = () => {
    console.log('logout');
    localStorage.removeItem('user');
    dispatch(logout());
  };

  const handleSubmitForm = (e: any) => {
    e.preventDefault();
    if (messageInput && messageInput.trim() !== '') {
      const message: Message = {
        content: messageInput.trim(),
        date: dayjs().format(),
        author: currentUser!.username,
      };

      dispatch(sendMessage(message));
    }

    setMessageInput('');
  };

  const handleChangeInput = (event: any) => {
    setMessageInput(event.target.value);
  };

  useEffect(() => {
    dispatch(getUsers());
    dispatch(getMessages());
  }, [dispatch]);

  // Add green dot for online users
  const usersWithOnlineData = useMemo(() => {
    if (users.length < 1) {
      return [];
    }

    return users
      .map((user) => ({
        ...user,
        online: onlineUsers.some(
          (onlineUsername) => onlineUsername === user.username
        ),
      }))
      .sort((a, b) => a.username.localeCompare(b.username));
  }, [users, onlineUsers]);

  // Add green dot for online users
  const reversedMessages = useMemo(() => {
    if (messages.length < 1) {
      return [];
    }

    return [...messages].reverse();
  }, [messages]);

  if (messagesLoading || usersLoading) {
    return (
      <div className='flex justify-center items-center h-screen bg-gray-100'>
        Loading...
      </div>
    );
  }

  return (
    <>
      <Nav onClick={handleLogoutClick} />
      <div className='flex m-0 content'>
        <Sidebar users={usersWithOnlineData} currentUser={currentUser} />
        <ChatArea
          messages={reversedMessages}
          messageInput={messageInput}
          handleSubmitForm={handleSubmitForm}
          handleChangeInput={handleChangeInput}
        />
      </div>
    </>
  );
};
